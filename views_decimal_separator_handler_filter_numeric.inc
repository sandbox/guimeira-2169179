<?php

/**
 * Simple filter that extends the standard numeric filter, adding special handling to 
 * decimal separators.
 */
class views_decimal_separator_handler_filter_numeric extends views_handler_filter_numeric {
  
  /**
   * Filter configuration.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['decimal_separator_config']['contains'] = array(
      'enabled' => array('default' => FALSE),
      'separator' => array('default' => 'standard'),
    );
    
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['decimal_separator_config'] = array(
      'enabled' => array(
        '#type' => 'checkbox',
        '#title' => t('Use Views Decimal Separator'),
        '#default_value' => $this->options['decimal_separator_config']['enabled'],
        '#description' => t('Enable Views Decimal Separator for this filter.'),
      ),
      'separator' => array(
        '#type' => 'select',
        '#title' => t('Decimal separator'),
        '#options' => array(
          'standard' => t('Decimal separator: dot (.) | Thousands separator: comma (,)'),
          'inverted' => t('Decimal separator: comma (,) | Thousands separator: dot (.)'),
        ),
        '#default_value' => $this->options['decimal_separator_config']['separator'],
        '#dependency' => array('edit-options-decimal-separator-config-enabled' => array(TRUE)),
      ),
    );
  }
  
  /**
   * This is the function that actually deals with the separators.
   * The goal is to convert a number with the desired separators to a database-friendly format (using only a dot as
   * decimal separator).
   * First we remove the thousands separator and then convert the decimal separator to a dot.
   * If this feature is not enabled, the function simply returns the string untouched.
   */
  function sanitize_separators($str) {
    $options = $this->options['decimal_separator_config'];
    
    if(!empty($options) && $options['enabled']) {
      if($options['separator'] == 'standard') {
        $decimal = '.';
        $thousands = ',';
      }
      elseif($options['separator'] == 'inverted') {
        $decimal = ',';
        $thousands = '.';
      }
      
      $str = str_replace($thousands, '', $str);
      $str = str_replace($decimal, '.', $str);
      return $str;
    }
    else {
      return $str;
    }
  }
  
  /**
   * Overriding the query() method from the standard numeric filter. Here we store the current value (with the
   * desired separators) and change them to the sanitized ones (using only a dot). Then the parent's query()
   * method is invoked to build the database query. After that, we restore the un-sanitized values.
   */
  function query() {
    $old_values = array();
    
    foreach($this->value as $key => $value) {
      $old_values[$key] = $value;
      $this->value[$key] = $this->sanitize_separators($value);
    }
    
    parent::query();
    
    foreach($this->value as $key => $value) {
      $this->value[$key] = $old_values[$key];
    }
  }
}
