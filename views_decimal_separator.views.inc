<?php

/**
 * Integration with the views module.
 * We replace the standard numeric filter by our filter with separator handling capabilities.
 */
function views_decimal_separator_views_data_alter(&$data) {
  foreach ($data as $data_type => $data_fields) {
    foreach ($data_fields as $fieldname => $data_field) {
      if (isset($data_field['filter']['handler']) && ($data_field['filter']['handler'] == 'views_handler_filter_numeric')) {
          $data[$data_type][$fieldname]['filter']['handler'] = 'views_decimal_separator_handler_filter_numeric';
      }
    }
  }
}